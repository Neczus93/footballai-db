SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `innodb`.`STADIUM`;
DROP TABLE IF EXISTS `innodb`.`TEAM`;
DROP TABLE IF EXISTS `innodb`.`SQUAD`;
DROP TABLE IF EXISTS `innodb`.`TABELLE`;
DROP TABLE IF EXISTS `innodb`.`TEAMDETAILS`;
DROP TABLE IF EXISTS `innodb`.`WEATHER`;
DROP TABLE IF EXISTS `innodb`.`CONDITION`;
DROP TABLE IF EXISTS `innodb`.`MATCH`;
DROP TABLE IF EXISTS `innodb`.`MATCHRESULT`;
DROP TABLE IF EXISTS `innodb`.`MATCHRESULTPREDICTED`;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `innodb`.`STADIUM` (
  `stadiumID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `country` VARCHAR(20) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `houseNr` VARCHAR(8) NOT NULL,
  `postalCode` VARCHAR(8) NOT NULL,
  
  PRIMARY KEY (`stadiumID`),
  UNIQUE INDEX `stadiumID_UNIQUE` (`stadiumID` ASC));
  
CREATE TABLE `innodb`.`TEAM` (
  `teamID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `stadiumID` INT NULL,
  
  PRIMARY KEY (`teamID`),
  UNIQUE INDEX `teamID_UNIQUE` (`teamID` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX (stadiumID),
  
  FOREIGN KEY (stadiumID)
      REFERENCES `innodb`.`STADIUM`(`stadiumID`));

CREATE TABLE `innodb`.`TABELLE` (
  `tabelleID` INT NOT NULL AUTO_INCREMENT,
  `played` INT NOT NULL,
  `won` INT NOT NULL,
  `draw` INT NOT NULL,
  `lost` INT NOT NULL,
  `GF` INT NOT NULL COMMENT 'Gool for awayTeam',
  `GA` INT NOT NULL COMMENT 'Gool from awayTeam',
  `points` INT NOT NULL,
  
  PRIMARY KEY (`tabelleID`),
  UNIQUE INDEX `tabelleID_UNIQUE` (`tabelleID` ASC));
  
CREATE TABLE `innodb`.`TEAMDETAILS` (
  `teamDetailID` INT NOT NULL AUTO_INCREMENT,
  `teamID` INT NULL,
  `tabelleID` INT NULL,
  `avgAge` DECIMAL(4,2) NOT NULL,
  `squadSize` INT NOT NULL,
  `marketValue` DECIMAL(10,0) NOT NULL,
  `marketValueTop18` DECIMAL(10,0) NOT NULL,
  `nationalTeamMembers` INT NOT NULL,
  
  PRIMARY KEY (`teamDetailID`),
  UNIQUE INDEX `teamDetailID_UNIQUE` (`teamDetailID` ASC),
  INDEX (teamID),
  INDEX (tabelleID),
  
  FOREIGN KEY (teamID)
      REFERENCES `innodb`.`TEAM`(`teamID`),
  FOREIGN KEY (tabelleID)
      REFERENCES `innodb`.`TABELLE`(`tabelleID`));
  
CREATE TABLE `innodb`.`SQUAD` (
  `squadID` INT NOT NULL AUTO_INCREMENT,
  `players` VARCHAR(200) NOT NULL,
  
  PRIMARY KEY (`squadID`),
  UNIQUE INDEX `squadID_UNIQUE` (`squadID` ASC));

-- TODO:
-- CREATE TABLE `innodb`.`MATCH` (
--  `matchID` INT NOT NULL AUTO_INCREMENT,
--  `weatherID` INT NOT NULL,
--  `homeTeamID` INT NOT NULL,
--  `awayTeamID` INT NOT NULL,
--  `date` DATETIME NOT NULL,
--  `timeHour` INT NOT NULL,
--  `timeMinute` INT NOT NULL,
--  `homeSquadID` INT NOT NULL,
--  `awaySquadID` INT NOT NULL,
--  `matchResultID` INT NOT NULL,
--  `matchResultPredictedID` INT NOT NULL,
--  PRIMARY KEY (`matchID`),
--  UNIQUE INDEX `matchID_UNIQUE` (`matchID` ASC));
  
CREATE TABLE `innodb`.`MATCHRESULT` (
  `matchResultID` INT NOT NULL AUTO_INCREMENT,
  `fthg` INT NULL COMMENT 'Teljes játékidő – hazai csapat góljainak száma',
  `ftag` INT NULL COMMENT 'Teljes játékidő – vendégcsapat góljainak száma',
  `ftr` VARCHAR(1) COMMENT 'Teljes játékidő eredménye (H=hazai csapat, D=döntetlen, A=vendégcsapat)',
  `hthg` INT NULL COMMENT 'Félidő – hazai csapat góljainak száma',
  `htag` INT NULL COMMENT 'Félidő – vendégcsapat góljainak száma',
  `htr` VARCHAR(1) COMMENT 'Félidő eredménye (H=hazai csapat, D=döntetlen, A=vendégcsapat)',
  `hs` INT NULL COMMENT 'Kapura lövések száma – hazai csapat',
  `aws` INT NULL COMMENT 'Kapura lövések száma – vendégcsapat',
  `hst` INT NULL COMMENT 'Kaput eltaláló lövések száma – hazai csapat',
  `ast` INT NULL COMMENT 'Kaput eltaláló lövések száma – vendégcsapat',
  `hhw` INT NULL COMMENT 'Kapufát eltaláló lövések száma – hazai csapat',
  `ahw` INT NULL COMMENT 'Kapufát eltaláló lövések száma – vendégcsapat',
  `hc` INT NULL COMMENT 'Szögletek száma – hazai csapat',
  `ac` INT NULL COMMENT 'Szögletek száma –vendégcsapat',
  `hf` INT NULL COMMENT 'Szabálytalanságok száma – hazai csapat',
  `af` INT NULL COMMENT 'Szabálytalanságok száma – vendégcsapat',
  `hfkc` INT NULL COMMENT 'Szabadrúgások száma – hazai csapat',
  `afkc` INT NULL COMMENT 'Szabadrúgások száma – vendégcsapat',
  `ho` INT NULL COMMENT 'Leshelyzetek száma – hazai csapat',
  `ao` INT NULL COMMENT 'Leshelyzetek száma – vendégcsapat',
  `hy` INT NULL COMMENT 'Sárgalapok száma – hazai csapat',
  `ay` INT NULL COMMENT 'Sárgalapok száma – vendégcsapat',
  `hr` INT NULL COMMENT 'Piroslapok száma – hazai csapat',
  `ar` INT NULL COMMENT 'Piroslapok száma – vendégcsapat',
  `hbp` INT NULL COMMENT 'Büntető pontok – hazai csapat (sárgalap = 10, piroslap = 25)',
  `abp` INT NULL COMMENT 'Büntető pontok – vendég csapat (sárgalap = 10, piroslap = 25)',
  
  PRIMARY KEY (`matchResultID`),
  UNIQUE INDEX `matchResultID_UNIQUE` (`matchResultID` ASC));


CREATE TABLE `innodb`.`MATCHRESULTPREDICTED` (
  `matchResultPredictedID` INT NOT NULL AUTO_INCREMENT,
  `fthg` INT NULL COMMENT 'Teljes játékidő – hazai csapat góljainak száma',
  `ftag` INT NULL COMMENT 'Teljes játékidő – vendégcsapat góljainak száma',
  `ftr` VARCHAR(1) COMMENT 'Teljes játékidő eredménye (H=hazai csapat, D=döntetlen, A=vendégcsapat)',
  `hthg` INT NULL COMMENT 'Félidő – hazai csapat góljainak száma',
  `htag` INT NULL COMMENT 'Félidő – vendégcsapat góljainak száma',
  `htr` VARCHAR(1) NULL COMMENT 'Félidő eredménye (H=hazai csapat, D=döntetlen, A=vendégcsapat)',
  `hs` INT NULL COMMENT 'Kapura lövések száma – hazai csapat',
  `aws` INT NULL COMMENT 'Kapura lövések száma – vendégcsapat',
  `hst` INT NULL COMMENT 'Kaput eltaláló lövések száma – hazai csapat',
  `ast` INT NULL COMMENT 'Kaput eltaláló lövések száma – vendégcsapat',
  `hhw` INT NULL COMMENT 'Kapufát eltaláló lövések száma – hazai csapat',
  `ahw` INT NULL COMMENT 'Kapufát eltaláló lövések száma – vendégcsapat',
  `hc` INT NULL COMMENT 'Szögletek száma – hazai csapat',
  `ac` INT NULL COMMENT 'Szögletek száma –vendégcsapat',
  `hf` INT NULL COMMENT 'Szabálytalanságok száma – hazai csapat',
  `af` INT NULL COMMENT 'Szabálytalanságok száma – vendégcsapat',
  `hfkc` INT NULL COMMENT 'Szabadrúgások száma – hazai csapat',
  `afkc` INT NULL COMMENT 'Szabadrúgások száma – vendégcsapat',
  `ho` INT NULL COMMENT 'Leshelyzetek száma – hazai csapat',
  `ao` INT NULL COMMENT 'Leshelyzetek száma – vendégcsapat',
  `hy` INT NULL COMMENT 'Sárgalapok száma – hazai csapat',
  `ay` INT NULL COMMENT 'Sárgalapok száma – vendégcsapat',
  `hr` INT NULL COMMENT 'Piroslapok száma – hazai csapat',
  `ar` INT NULL COMMENT 'Piroslapok száma – vendégcsapat',
  `hbp` INT NULL COMMENT 'Büntető pontok – hazai csapat (sárgalap = 10, piroslap = 25)',
  `abp` INT NULL COMMENT 'Büntető pontok – vendég csapat (sárgalap = 10, piroslap = 25)',
  
  PRIMARY KEY (`matchResultPredictedID`),
  UNIQUE INDEX `matchResultPredictedID_UNIQUE` (`matchResultPredictedID` ASC));
  
CREATE TABLE `innodb`.`CONDITION` (
  `conditionID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `value` DECIMAL(2,1) NOT NULL,
  
  PRIMARY KEY (`conditionID`),
  UNIQUE INDEX `conditionID_UNIQUE` (`conditionID` ASC));
  
CREATE TABLE `innodb`.`WEATHER` (
  `weatherID` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `temperature` DECIMAL(5,2) NOT NULL,
  `dewPoint` DECIMAL(5,2) NOT NULL,
  `humidity` DECIMAL(5,2) NOT NULL,
  `windDirect` DECIMAL(5,2) NOT NULL,
  `windSpeed` DECIMAL(5,2) NOT NULL,
  `presure` DECIMAL(5,2) NOT NULL,
  `conditionID` INT NULL,
  
  PRIMARY KEY (`weatherID`),
  UNIQUE INDEX `weatherID_UNIQUE` (`weatherID` ASC),
  INDEX (conditionID),
  
  FOREIGN KEY (conditionID)
      REFERENCES `innodb`.`CONDITION`(`conditionID`));

-- without NN 
CREATE TABLE `innodb`.`MATCH` (
  `matchID` INT NOT NULL AUTO_INCREMENT,
  `weatherID` INT,
  `homeTeamID` INT,
  `awayTeamID` INT,
  `date` DATE,
  `timeHour` INT,
  `timeMinute` INT,
  `homeSquadID` INT,
  `awaySquadID` INT,
  `matchResultID` INT,
  `matchResultPredictedID` INT,
  
  PRIMARY KEY (`matchID`),
  UNIQUE INDEX `matchID_UNIQUE` (`matchID` ASC),
  INDEX (weatherID),
  INDEX (homeTeamID),
  INDEX (awayTeamID),
  INDEX (homeSquadID),
  INDEX (awaySquadID),
  INDEX (matchResultID),
  INDEX (matchResultPredictedID),
  
  FOREIGN KEY (weatherID)
      REFERENCES `innodb`.`WEATHER`(`weatherID`),
  FOREIGN KEY (homeTeamID)
      REFERENCES `innodb`.`TEAMDETAILS`(`teamDetailID`),
  FOREIGN KEY (awayTeamID)
      REFERENCES `innodb`.`TEAMDETAILS`(`teamDetailID`),
  FOREIGN KEY (homeSquadID)
      REFERENCES `innodb`.`SQUAD`(`squadID`),
  FOREIGN KEY (awaySquadID)
      REFERENCES `innodb`.`SQUAD`(`squadID`),
  FOREIGN KEY (matchResultID)
      REFERENCES `innodb`.`MATCHRESULT`(`matchResultID`),
  FOREIGN KEY (matchResultPredictedID)
      REFERENCES `innodb`.`MATCHRESULTPREDICTED`(`matchResultPredictedID`));
  
-- First import CVS into database!
-- just change the FROMtable name!
INSERT INTO innodb.`MATCHRESULT`(
	`fthg`,
	`ftag`,
  	`ftr`,
	`hthg`,
	`htag`,
	`htr`,
	`hs`,
	`aws`,
	`hst`,
	`ast`,
	`hc`,
	`ac`,
	`hf`,
	`af`,
	`hy`,
	`ay`,
	`hr`,
	`ar`
)
SELECT 
	FTHG,
    FTAG,
    FTR,
    HTHG,
    HTAG,
    HTR,
    HS,
    AWS,
    HST,
    AST,
    HC,
    AC,
    HF,
    AF,
    HY,
    AY,
    HR,
    AR
FROM innodb.`england1-2019`;

-- Add imported data to match table
USE innodb;
INSERT INTO innodb.`MATCH`(
	`date`,
	`timeHour`,
	`timeMinute`
)
SELECT
	STR_TO_DATE(`Date`,'%d/%m/%Y') AS `date`, 
	SUBSTR(`Time`,1,2) AS `timeHour`,
    SUBSTR(`Time`,4,2) AS `timeMinute`
FROM innodb.`england1-2019`;

UPDATE
    innodb.`MATCH` m,
    innodb.`MATCHRESULT` mr
SET
    m.`matchResultID` = mr.`matchResultID`
WHERE
    m.`matchID` = mr.`matchResultID`;

-- Add imported data to team table
USE innodb;
INSERT INTO innodb.`TEAM`(
	`name`
)
SELECT DISTINCT HomeTeam FROM innodb.`england1-2019`;

USE innodb;
INSERT INTO innodb.`TEAM`(
	`name`
)
SELECT DISTINCT HomeTeam 
FROM innodb.`england1-2019`
WHERE 
	NOT EXISTS 
		(	SELECT `name` 
			FROM innodb.`TEAM`
			WHERE innodb.`TEAM`.name = innodb.`england1-2019`.HomeTeam);
			
-- First import CVS into database!
-- just change the FROMtable name!
INSERT INTO innodb.`MATCHRESULT`(
	`fthg`,
	`ftag`,
  	`ftr`,
	`hthg`,
	`htag`,
	`htr`,
	`hs`,
	`aws`,
	`hst`,
	`ast`,
	`hc`,
	`ac`,
	`hf`,
	`af`,
	`hy`,
	`ay`,
	`hr`,
	`ar`
)
SELECT 
	FTHG,
    FTAG,
    FTR,
    HTHG,
    HTAG,
    HTR,
    HS,
    AWS,
    HST,
    AST,
    HC,
    AC,
    HF,
    AF,
    HY,
    AY,
    HR,
    AR
FROM innodb.`england1-2018`;

-- Add imported data to match table
USE innodb;
INSERT INTO innodb.`MATCH`(
	`date`
)
SELECT
	STR_TO_DATE(`Date`,'%d/%m/%Y') AS `date`
FROM innodb.`england1-2018`;

UPDATE
    innodb.`MATCH` m,
    innodb.`MATCHRESULT` mr
SET
    m.`matchResultID` = mr.`matchResultID`
WHERE
    m.`matchID` = mr.`matchResultID`;

-- Add imported data to team table
USE innodb;
INSERT INTO innodb.`TEAM`(
	`name`
)
SELECT DISTINCT HomeTeam 
FROM innodb.`england1-2018`
WHERE 
	NOT EXISTS 
		(	SELECT `name` 
			FROM innodb.`TEAM`
			WHERE innodb.`TEAM`.name = innodb.`england1-2018`.HomeTeam);